// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';
var twilio = require('twilio');
var accountSid = "ACdce0f429f9e83f79a8713875fe2d937a";
var authToken = "dd07741529822d48691d715674ba6d80";
const twilioClient = new twilio(accountSid, authToken);

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const {result} = context;
    console.log(result['to'])
    console.log(result['message'])
    var to = result['to'].substr(0,1) == "+" ? result['to'] : `+1${result['to']}`;
    console.log(to);
    
     let messageResponse = await twilioClient.messages.create({
       body: result['message'],
       from: '+16122611453',
       to
     });
     
    console.log(messageResponse);
    
    
    return context;
  };
}
