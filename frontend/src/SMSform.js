import React, { Component } from 'react';
import client from './feathers';

class SMSform extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  sendSMS(ev) {
    const inputTo = ev.target.querySelector('[id="to"]');
    const inputMessage = ev.target.querySelector('[id="message"]');
    const to = inputTo.value.trim();
    const message = inputMessage.value.trim();
    if(to=="") inputMessage.classList.add('is-invalid');
    if(message=="") inputMessage.classList.add('is-invalid');
    
    console.log( "To: " + to );
    console.log( "Message: " + message );

    this.state.sms.create({
      to,message
    })
    .then(() => {
      inputTo.value = '';
      inputMessage.value = '';
      inputMessage.classList.remove('is-invalid');
      inputMessage.classList.remove('is-invalid');
    });
    
    ev.preventDefault();
  }

  componentDidMount() {
    const sms = client.service('sms');

    this.setState({sms})
  }
  
  render() {
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Send SMS</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <h4 className="mb-3">Phone Number</h4>
          <form onSubmit={this.sendSMS.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-12 mb-3">
                <label htmlFor="to">To</label>
                <input type="text" className="form-control" id="to" defaultValue="" required />
                <div className="invalid-feedback">
                    Phone Number is required.
                </div>
                <label htmlFor="message">Message:</label>
                <input type="text" className="form-control invalid" id="message" defaultValue="" required />
                <div className="invalid-feedback">
                    Message is required.
                </div>
              </div>
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Send SMS</button>
          </form>
        </div>
      </div>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default SMSform;
